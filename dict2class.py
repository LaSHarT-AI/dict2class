# !/usr/bin/python3.8
# -*- coding: UTF-8 -*-
#=============================================================================
# The MIT License
# 
# Copyright (c) 2020 LaSHarT-AI, (❤ LaSHarT ❤#4564)
# Server Support discord:  NaN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# =============================================================================
"""
Dict2Class est une implementation pour la création d'objets sous forme de list.
Pour des fichiers yaml/js/md/etc "charger".


Example d'utilisation:

    
    >>> import os
    >>> import yaml
    
    Création de la fonction
    >>> def load_file_yaml(trust_file: str):
    >>>     if not os.path.isfile(trust_file):
    >>>         raise os.error("Une erreur est survenue !")
    >>>     with open(trust_file, "r") as fs:
    >>>         load_file = yaml.load(fs)
    >>>     return Dict2Class(load_file)

    Utilisation :
    >>> abs_path = os.path.dirname(os.path.abspath(__file__))
    >>> load_file = load_file_yaml(abs_path + "/config.yaml")
    >>> print(load_file[*])


Author:
    LaSHarT-AI

"""

def Dict2Class(_dit: dict):
    """Class que nous retournont le contenue sous forme de list !
    
    return: list_oject
    """
    class DBClass:
        def __getattribute__(self, item):
            self.__getattr__(item)

    for key in _dit:
        setattr(DBClass, key, _dit[key])

    return DBClass
